<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabla de Multiplicar</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<?php

// verifica que el usuario haya mandado un límite
if (!isset($_GET['limite'])) {
    echo "<h2>ERROR! Debe ingresar un límite</h2>";
    die;
}

// toma el valor pasado por GET
$limite = $_GET['limite'];

// imprime la tabla de multiplicar
echo "<table>";
for ($fila=0; $fila<=$limite; $fila++) {
    echo "<tr>";
    for ($col=0; $col<=$limite; $col++) {
        if ($col == 0)
            echo "<td class='resaltado'>" . $fila . "</td>";
        elseif ($fila == 0)
            echo "<td class='resaltado'>" . $col . "</td>";
        elseif ($fila == $col)
            echo "<td class='resaltado'>" . ($fila*$col) . "</td>";
        else
            echo "<td>" . ($fila*$col) . "</td>";
    }
    echo "</tr>";
}
echo "</table>";

?>

</body>
</html>